FROM node

WORKDIR /calltest

ADD . /calltest

EXPOSE 3000

CMD ["npm", "start"]

